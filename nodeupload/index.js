const express = require('express');
const fileUpload = require('express-fileupload');

const app = express();

app.use(fileUpload());

app.post('/upload', (req, res) => {
  if (!req.files) {
    return res.status(400).send('No files were uploaded.');
  }
  const { uploadedFile } = req.files;
  uploadedFile.mv(`../../temp/${uploadedFile.name}`, (err) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.send('File uploaded!');
  });
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
