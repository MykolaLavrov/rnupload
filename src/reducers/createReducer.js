import { combineReducers } from 'redux';

import reducerApp from './reducerApp';

const reducers = {
  reducerApp,
};

const appReducer = combineReducers(reducers);

const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;
