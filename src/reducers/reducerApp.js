import * as types from '../actions/actionsApp';

export const initialState = {
  user: 'user1',
  selectedFile: null,
  isUploading: false,
  progress: 0,
};

export default function reducerApp(state = initialState, action) {
  switch (action.type) {
    case types.SET_USER: {
      return {
        ...state,
        user: action.data,
      };
    }
    case types.SET_SELECTED_FILE: {
      return {
        ...state,
        selectedFile: action.data,
      };
    }
    case types.SET_UPLOAD_IN_PROGRESS: {
      return {
        ...state,
        isUploading: action.isUploading,
        progress: action.progress,
      };
    }

    default:
      return state;
  }
}
