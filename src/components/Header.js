import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import { width, headerHeight, headerPaddingTop } from '../constants/Layouts';
import { typeTitle } from '../constants/Fonts';

const styles = StyleSheet.create({
  container: {
    height: headerHeight,
    paddingTop: headerPaddingTop,
    width,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});

const Header = ({ title }) => (
  <View style={styles.container}>
    <Text style={typeTitle}>{title}</Text>
  </View>
);

export default Header;
