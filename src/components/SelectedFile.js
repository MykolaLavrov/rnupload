import React from 'react';
import {
  Button,
  Image,
  View,
  Text,
  StyleSheet,
} from 'react-native';

import { width, paddingRegular } from '../constants/Layouts';
import { typeRegularText, smallText } from '../constants/Fonts';

const itemHeight = 70;

const styles = StyleSheet.create({
  container: {
    height: itemHeight,
    width,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  preview: {
    width: itemHeight,
    height: itemHeight,
    marginRight: paddingRegular,
  },
  infoContainer: {
    justifyContent: 'space-around',
    height: itemHeight,
    width: width - itemHeight * 2 - paddingRegular,
  },
  buttonContainer: {
    height: itemHeight,
    width: 80,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingRight: paddingRegular,
  },
});

const SelectedFile = ({
  data: {
    fileName, uri, fileSize, hash,
  }, onPressSend,
}) => (
  <View style={styles.container}>
    <Image
      resizeMode="cover"
      source={{ uri }}
      style={styles.preview}
    />
    <View style={styles.infoContainer}>
      <Text style={typeRegularText}>{fileName}</Text>
      <Text style={typeRegularText}>
        {fileSize}
        {' '}
          bytes
      </Text>
      <Text style={smallText} numberOfLines={3}>
        sha256
        {' '}
        {hash}
      </Text>
    </View>
    <View style={styles.buttonContainer}>
      <Button title="Send" onPress={onPressSend} />
    </View>
  </View>
);

export default SelectedFile;
