import React from 'react';
import { Provider } from 'react-redux';
import store from './reducers/createStore';
import Home from './containers/ContainerHome';

// console.disableYellowBox = true;

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Home />
      </Provider>
    );
  }
}
