import apisauce from 'apisauce';
import { baseURL } from '../constants/requestUrl';

const api = apisauce.create({
  baseURL,
  headers: {
    Accept: 'application/json',
    'Content-type': 'application/json',
  },
  timeout: 1000,
});

const setTokenToHeaders = (token) => {
  api.setHeaders({
    Authorization: token ? `Bearer ${token}` : null,
  });
};

export const uploadFile = ({ uri, fileName, onUploadProgressCallback }) => {
  const body = new FormData();
  body.append('uploadedFile', {
    name: fileName,
    uri,
  });
  const headers = {
    'Content-Type': 'multipart/form-data',
  };
  return api.post('upload', body, { headers, onUploadProgress: value => onUploadProgressCallback(value) });
};
