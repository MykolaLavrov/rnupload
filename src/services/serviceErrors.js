import { Alert } from 'react-native';

export const commonErrorMessage = 'Sorry, something went wrong';

export const handleError = ({
  error, method, errorData, errorCode,
}) => {
  try {
    console.warn('handleError', error, errorData, errorCode);
    let errorString;
    if (typeof error === 'string') {
      errorString = error.replace(/_/gi, ' ');
    } else {
      errorString = JSON.stringify(error);
    }
    Alert.alert(errorString);
    console.warn(`Error in method ${method}: ${errorString}`);
  } catch (e) {
    console.warn(`Error in method handleError: ${e}`);
  }
};

export const handleNetworkError = ({ method, response }) => {
  let hasErrors = false;
  if (!response) return hasErrors;
  const { ok, problem, data } = response;
  if (!ok) {
    handleError({ error: problem, method });
    hasErrors = true;
  } else if (!data) {
    handleError({ error: commonErrorMessage, method });
    hasErrors = true;
  }
  if (hasErrors) console.warn('handleNetworkError', response);
  return hasErrors;
};
