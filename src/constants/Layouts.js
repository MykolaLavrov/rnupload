import { Dimensions, Platform } from 'react-native';

export const { width, height } = Dimensions.get('window');

export const headerHeight = Platform.OS === 'ios' ? 70 : 50;
export const headerPaddingTop = Platform.OS === 'ios' ? 20 : 0;

export const paddingRegular = 16;
