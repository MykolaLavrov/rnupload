const Colors = {
  modalBackground: 'rgba(0,0,0,0.3)',
  white: '#fff',
  blue: 'blue',
};

export const {
  modalBackground,
  white,
  blue,
} = Colors;
