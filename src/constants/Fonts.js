import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  typeTitle: {
    fontSize: 18,
    fontWeight: '600',
  },
  typeRegularText: {
    fontSize: 14,
  },
  smallText: {
    fontSize: 8,
  },
});

export const { typeTitle, typeRegularText, smallText } = styles;
