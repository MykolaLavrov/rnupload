import React from 'react';
import {
  View,
  StyleSheet,
  Button,
  Modal,
} from 'react-native';

import Header from './components/Header';
import SelectedFile from './components/SelectedFile';

import { width, paddingRegular } from './constants/Layouts';
import { modalBackground, white, blue } from './constants/Colors';

const progressBarWidth = 300;
const progressBarHeight = 15;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  footer: {
    height: 70,
    width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: paddingRegular,
    position: 'absolute',
    bottom: 0,
    left: 0,
  },
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: modalBackground,
  },
  progressContainer: {
    width: progressBarWidth,
    height: progressBarHeight,
    borderRadius: progressBarHeight / 2,
    borderWidth: 1,
    borderColor: white,
  },
  progress: {
    height: progressBarHeight - 2,
    borderRadius: (progressBarHeight - 2) / 2,
    backgroundColor: blue,
  },
});

class Home extends React.Component {
  render() {
    const {
      selectFile,
      selectedFile,
      sendFile,
      isUploading,
      progress,
    } = this.props;
    return (
      <View style={styles.container}>
        <Header title="Upload File to Cloud" />
        {
          selectedFile
            && (
            <SelectedFile
              data={selectedFile}
              onPressSend={() => sendFile({
                uri: selectedFile?.uri,
                fileName: selectedFile?.fileName,
                fileSize: selectedFile?.fileSize,
              })}
            />
            )
        }
        <View style={styles.footer}>
          <Button title="Select Photo" onPress={() => selectFile({ fileType: 'photo' })} />
          <Button title="Select Video" onPress={() => selectFile({ fileType: 'video' })} />
        </View>
        <Modal
          animationType="none"
          transparent
          visible={isUploading}
          onRequestClose={() => {}}
        >
          <View style={styles.modalContainer}>
            <View style={styles.progressContainer}>
              <View style={[styles.progress, { width: (progressBarWidth - 2) * progress }]} />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default Home;
