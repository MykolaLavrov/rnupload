import { connect } from 'react-redux';

import Home from '../home';
import { selectFile, sendFile } from '../actions/actionsApp';

const mapStateToProps = ({
  reducerApp: {
    selectedFile,
    isUploading,
    progress,
  },
}) => ({
  selectedFile,
  isUploading,
  progress,
});

const mapDispatchToProps = {
  selectFile,
  sendFile,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
