import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { uploadFile } from '../services/serviceREST';
import {
  handleError,
  handleNetworkError,
  commonErrorMessage,
} from '../services/serviceErrors';

export const SET_USER = 'SET_USER';
export const SET_SELECTED_FILE = 'SET_SELECTED_FILE';
export const SET_UPLOAD_IN_PROGRESS = 'SET_UPLOAD_IN_PROGRESS';

export const setUser = data => ({
  type: SET_USER,
  data,
});

export const setSelectedFile = data => ({
  type: SET_SELECTED_FILE,
  data,
});

export const selectFile = ({ fileType }) => (dispatch) => {
  let mediaType;
  switch (fileType) {
    case 'photo':
      mediaType = 'photo';
      break;
    case 'video':
      mediaType = 'video';
      break;
    default:
      mediaType = 'mixed';
  }

  const options = {
    noData: true,
    mediaType,
  };

  ImagePicker.launchImageLibrary(options, (response) => {
    if (response.didCancel) {
      console.warn('User cancelled image picker');
    } else if (response.error) {
      console.warn('ImagePicker Error: ', response.error);
    } else {
      dispatch(setSelectedFile(null));
      dispatch(setSelectedFile(response));
      dispatch(getHash({ path: response.uri.replace('file://', '') }));
    }
  });
};

export const getHash = ({ path }) => async (dispatch, getState) => {
  try {
    const hash = await RNFetchBlob.fs.hash(path, 'sha256');
    if (hash) {
      const { selectedFile } = getState().reducerApp;
      const selectedFileWithHash = { ...selectedFile, hash };
      dispatch(setSelectedFile(selectedFileWithHash));
    }
  } catch (errorData) {
    handleError({ error: commonErrorMessage, errorData });
  }
};

export const setUploadInProgress = ({ isUploading, progress }) => ({
  type: SET_UPLOAD_IN_PROGRESS,
  isUploading,
  progress,
});

export const sendFile = ({ uri, fileName }) => async (dispatch) => {
  dispatch(setUploadInProgress({ isUploading: true, progress: 0 }));
  try {
    const response = await uploadFile({
      uri,
      fileName,
      onUploadProgressCallback: ({ loaded, total }) => {
        dispatch(setUploadInProgress({ isUploading: true, progress: loaded / total }));
      },
    });
    dispatch(setUploadInProgress({ isUploading: false, progress: 0 }));
    setTimeout(() => {
      if (handleNetworkError({ response, method: 'action sendFile' })) return;
      if (!response.ok) {
        console.warn('sendFile error', response.problem);
      }
    }, 0);
  } catch (error) {
    console.warn('sendFile error', error);
    dispatch(setUploadInProgress({ isUploading: false, progress: 0 }));
  }
};
